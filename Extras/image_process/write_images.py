from sklearn import cross_validation, datasets
import glob
import os
import cv2
import matplotlib.image as img


def writeimages(image_list,out_dir):
    
    if not os.path.exists(out_dir):
    	os.makedirs(out_dir)
    
    for i in range(0,len(image_list)):
        
        filepath_old = image_list[i]
        image = cv2.imread(filepath_old)
        
        filepath_new = out_dir
        temp = filepath_old.split('/')
        filepath_new = filepath_new + '/' + temp[len(temp)-2]
        if not os.path.exists(filepath_new):
            os.makedirs(filepath_new)
        filepath_new = filepath_new + '/' + temp[len(temp)-1]
        print("Writing at: " + filepath_new)
        cv2.imwrite(filepath_new,image)


train_data_dir = '/home/yash/facenet-model/datasets/lfw/lfw_mtcnnpy_160'
train_out = '/home/yash/Datasets/lfw_div/trains'
validation_out = '/home/yash/Datasets/lfw_div/validation'
test_out = '/home/yash/Datasets/lfw_div/test'

rasterList = glob.glob(train_data_dir + '/*/*.jpg')
train_samples, test_samples = cross_validation.train_test_split(rasterList, test_size=0.4)
#train_samples, validation_samples = cross_validation.train_test_split(train_samples, test_size=0.3)

print("Writing Train Images..")
writeimages(train_samples,train_out)
#print("Train Images done. Writing Validation Images..")
#writeimages(validation_samples,validation_out)
print("Validation Images done. Writing Test Images..")
writeimages(test_samples,test_out)

print("Process Done.")