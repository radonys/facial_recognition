from datetime import datetime
from scipy import misc
import os.path
import time
import sys
import random
import numpy as np
import importlib
import argparse
import h5py

class ImageClass():

	"Stores the paths to images for a given class"

	def __init__(self, name, image_paths):
		self.name = name
		self.image_paths = image_paths
  	
  	def __str__(self):
  		return self.name + ', ' + str(len(self.image_paths)) + ' images'
  
  	def __len__(self):
  		return len(self.image_paths)

def get_image_paths(facedir):

	image_paths = []

	if os.path.isdir(facedir):

		images = os.listdir(facedir)
		image_paths = [os.path.join(facedir,img) for img in images]

	return image_paths

def write_images(dataset, output_dir):

	nrof_images_total = 0
	index = 0

	for clas in dataset:
	
		output_class_dir = os.path.join(output_dir, clas.name)

		if not os.path.exists(output_class_dir):
			os.makedirs(output_class_dir)

		random.shuffle(clas.image_paths)

		print dataset[index]
		index = index + 1

		for image_path in clas.image_paths:

			nrof_images_total += 1
			filename = os.path.splitext(os.path.split(image_path)[1])[0]
			output_filename = os.path.join(output_class_dir, filename+'.png')

			print(image_path)
			img = misc.imread(image_path)
			misc.imsave(output_filename, img)

			'''if not os.path.exists(output_filename):

			else:
				print('Unable to write "%s"' % image_path)'''

	print('Total number of images: %d' % nrof_images_total)

def get_dataset(paths):

	dataset = []
	
	for path in paths.split(':'):
		
		path_exp = os.path.expanduser(path)
		classes = os.listdir(path_exp)
		classes.sort()
		nrof_classes = len(classes)
        
        for i in range(nrof_classes):

        	class_name = classes[i]
        	facedir = os.path.join(path_exp, class_name)
        	image_paths = get_image_paths(facedir)
        	dataset.append(ImageClass(class_name, image_paths))
  
	return dataset

def find_threshold(var, percentile):

	hist, bin_edges = np.histogram(var, 100)
	cdf = np.float32(np.cumsum(hist)) / np.sum(hist)
	bin_centers = (bin_edges[:-1]+bin_edges[1:])/2
	threshold = np.interp(percentile*0.01, cdf, bin_centers)
	
	return threshold

def filter_dataset(dataset, data_filename, percentile, min_nrof_images_per_class, type):

	with h5py.File(data_filename,'r') as f:

		distance_to_center = np.array(f.get('distance_to_center'))
		label_list = np.array(f.get('label_list'))
		image_list = np.array(f.get('image_list'))
		distance_to_center_threshold = find_threshold(distance_to_center, percentile)

		print distance_to_center_threshold

		if type==1:
			indices = np.where(distance_to_center>=distance_to_center_threshold)[0]
			print "1"

		else:
			indices = np.where(distance_to_center<distance_to_center_threshold)[0]
			print "2"

		filtered_dataset = dataset
		removelist = []
        
        for i in indices:

        	label = label_list[i]
        	image = image_list[i]

        	if image in filtered_dataset[label].image_paths:
        		filtered_dataset[label].image_paths.remove(image)
			
			if len(filtered_dataset[label].image_paths)<min_nrof_images_per_class:
				removelist.append(label)

		ix = sorted(list(set(removelist)), reverse=True)
		for i in ix:
			del(filtered_dataset[i])

	'''index = 0

	for i in filtered_dataset:
		print filtered_dataset[index]
		index = index + 1'''

	print "Dataset Filtered Type: ", type

	return filtered_dataset

def main(args):
	
	train_set = get_dataset(args.data_dir)
	
	out = os.path.expanduser(args.out_dir)

	if not os.path.exists(out):
		os.makedirs(out)

	out_correct = out + '/Correct/'
	out_incorrect = out + '/Incorrect/'

	os.makedirs(out_correct)
	os.makedirs(out_incorrect)

	if args.filter_filename:
	
		train_set_correct = filter_dataset(train_set, os.path.expanduser(args.filter_filename), args.filter_percentile, args.filter_min_nrof_images_per_class, 2)
		print len(train_set_correct)

		'''index = 0

		for i in train_set_correct:
			print train_set_correct[index]
			index = index + 1'''
		
		'''train_set_incorrect = filter_dataset(train_set, os.path.expanduser(args.filter_filename), args.filter_percentile, args.filter_min_nrof_images_per_class, 2)
		print len(train_set_incorrect)'''

		print "Writing Images"

		write_images(train_set_correct, out_correct)
		#write_images(train_set_incorrect, out_incorrect)

		print "Process Completed."

	else:
		print "File not found."

def parse_arguments(argv):
	
	parser = argparse.ArgumentParser()
	
	parser.add_argument('--data_dir', type=str, help='Path to the data directory containing aligned face patches. Multiple directories are separated with colon.', default='~/datasets/casia/casia_maxpy_mtcnnalign_182_160')  
	parser.add_argument('--filter_filename', type=str, help='File containing image data used for dataset filtering', default='~/facenet-model/filtering_results/1.hdf')
	parser.add_argument('--filter_percentile', type=float, help='Keep only the percentile images closed to its class center', default=100.0)
	parser.add_argument('--filter_min_nrof_images_per_class', type=int, help='Keep only the classes with this number of examples or more', default=0)
	parser.add_argument('--out_dir', type=str, help='Output Directory', default='~/datasets/out')

	return parser.parse_args(argv)

if __name__ == '__main__':
	
	main(parse_arguments(sys.argv[1:]))