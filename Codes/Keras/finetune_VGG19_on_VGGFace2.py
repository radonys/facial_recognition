import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.optimizers import SGD
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras import backend as K
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from tflearn.data_utils import image_preloader
from tflearn.data_utils import build_hdf5_image_dataset
import h5py
import numpy as np
from sklearn import cross_validation, datasets
import glob
import os
from sklearn.metrics import log_loss
from keras.utils import plot_model

def vgg19_model(img_rows, img_cols, channel=1, num_classes=None):

    print("Downloading pretrained model...")
    base_model = keras.applications.vgg19.VGG19(include_top=False, weights='imagenet', input_shape=(img_rows, img_cols, channel), pooling='max')
    print("Download Complete. Building model..")
    x = base_model.output
    x = Dense(1024, activation='relu')(x)
    x = Dropout(0.5)(x)
    predictions = Dense(num_classes, activation='softmax')(x)

    model = Model(inputs=base_model.input, outputs=predictions)

    # train only the top layers (which were randomly initialized)
    for layer in base_model.layers:
        layer.trainable = False

    # Learning rate is changed to 0.001
    sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])

    return model

if __name__ == '__main__':
    
    # Example to fine-tune on 3000 samples from Cifar10

    img_rows, img_cols = 224, 224 # Resolution of inputs
    channel = 3
    num_classes = 8631
    batch_size = 32
    nb_epoch = 1000
    nb_steps = 1000
    train_data_dir = '/home/yash/Datasets/VGG-Face2/trains'
    validation_data_dir = '/home/yash/Datasets/VGG-Face2/validation'
    test_data_dir = '/home/yash/Datasets/VGG-Face2/test'

    # Load our model
    model = vgg19_model(img_rows, img_cols, channel, num_classes)

    #Image Input - Direct/Generator
    print("Images Loading")

    #Direct Input
    '''
    #build_hdf5_image_dataset(train_data_dir, image_shape=(224, 224), mode='folder', output_path='dataset_train.h5', categorical_labels=True, normalize=True, files_extension=['.jpg', '.png'])
    h5f = h5py.File('dataset_train.h5', 'r')
    X = h5f['X']
    X = np.array(X)
    Y = h5f['Y']
    Y = np.array(Y)

    print("Training Images loaded. Test Images Loading..")

    #build_hdf5_image_dataset(test_data_dir, image_shape=(224, 224), mode='folder', output_path='dataset_test.h5', categorical_labels=True, normalize=True, files_extension=['.jpg', '.png'])
    h5f = h5py.File('dataset_test.h5', 'r')
    Xtest = h5f['X']
    Xtest = np.array(Xtest)
    Ytest = h5f['Y']
    Ytest = np.array(Ytest)
    '''

    #Generator Input
    all_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = all_datagen.flow_from_directory(train_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')
    validation_generator = all_datagen.flow_from_directory(validation_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')
    test_generator = all_datagen.flow_from_directory(test_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')

    print("Images Loaded.")

    # Start Fine-tuning
    print("Training Started")
    #plot_model(model, to_file='/home/yash/Facial_Recognition/VGG/19/checkpoints/vggface2/model.png', show_shapes=True)
    tbCallBack = keras.callbacks.TensorBoard(log_dir='/home/yash/Facial_Recognition/VGG/19/logs/vggface2', histogram_freq=0, write_graph=True, write_images=True)
    checkCallBack = keras.callbacks.ModelCheckpoint(filepath='/home/yash/Facial_Recognition/VGG/19/checkpoints/vggface2/weights.hdf5', verbose=1, save_best_only=True, period=1)
    history = model.fit_generator(train_generator, steps_per_epoch=nb_steps, epochs= nb_epoch, verbose=2, callbacks=[tbCallBack,checkCallBack], validation_data=validation_generator, validation_steps=nb_steps)
    #hist = model.fit(x=X, y=Y, batch_size=batch_size, epochs=nb_epoch, verbose=2, callbacks=[tbCallBack], validation_split=0.1, shuffle=True, steps_per_epoch=None, validation_steps=None)
    print(history.history.keys())
    model.save('/home/yash/Facial_Recognition/VGG/19/checkpoints/vggface2/vggface2_vgg19.h5')
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('/home/yash/Facial_Recognition/VGG/19/checkpoints/vggface2/accuracy.png')
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('/home/yash/Facial_Recognition/VGG/19/checkpoints/vggface2/loss.png')
    print("Training Done. Evaluation Started..")
    scores = model.evaluate_generator(validation_generator, steps=nb_steps*10, max_queue_size=10, workers=0, use_multiprocessing=False)
    #scores = model.evaluate(x=Xtest, y=Ytest, batch_size=32, verbose=2, sample_weight=None, steps=None)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    print("All Done.")