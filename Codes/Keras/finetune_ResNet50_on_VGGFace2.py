# -*- coding: utf-8 -*-

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import keras
from keras.models import Sequential
from keras.optimizers import SGD
from keras.layers import Input, Dense, Convolution2D, MaxPooling2D, AveragePooling2D, ZeroPadding2D, Dropout, Flatten, merge, Reshape, Activation
from keras.layers.normalization import BatchNormalization
from keras.models import Model
from keras import backend as K
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from tflearn.data_utils import image_preloader
from tflearn.data_utils import build_hdf5_image_dataset
import h5py
import numpy as np
from sklearn import cross_validation, datasets
import glob
import os
from sklearn.metrics import log_loss
from keras.utils import plot_model

def identity_block(input_tensor, kernel_size, filters, stage, block):
    """
    The identity_block is the block that has no conv layer at shortcut
    Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the nb_filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    """

    nb_filter1, nb_filter2, nb_filter3 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Convolution2D(nb_filter1, 1, 1, name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter2, kernel_size, kernel_size,
                      border_mode='same', name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter3, 1, 1, name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    x = merge([x, input_tensor], mode='sum')
    x = Activation('relu')(x)
    return x

def conv_block(input_tensor, kernel_size, filters, stage, block, strides=(2, 2)):
    """
    conv_block is the block that has a conv layer at shortcut
    # Arguments
        input_tensor: input tensor
        kernel_size: defualt 3, the kernel size of middle conv layer at main path
        filters: list of integers, the nb_filters of 3 conv layer at main path
        stage: integer, current stage label, used for generating layer names
        block: 'a','b'..., current block label, used for generating layer names
    Note that from stage 3, the first conv layer at main path is with subsample=(2,2)
    And the shortcut should have subsample=(2,2) as well
    """

    nb_filter1, nb_filter2, nb_filter3 = filters
    conv_name_base = 'res' + str(stage) + block + '_branch'
    bn_name_base = 'bn' + str(stage) + block + '_branch'

    x = Convolution2D(nb_filter1, 1, 1, subsample=strides,
                      name=conv_name_base + '2a')(input_tensor)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2a')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter2, kernel_size, kernel_size, border_mode='same',
                      name=conv_name_base + '2b')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2b')(x)
    x = Activation('relu')(x)

    x = Convolution2D(nb_filter3, 1, 1, name=conv_name_base + '2c')(x)
    x = BatchNormalization(axis=bn_axis, name=bn_name_base + '2c')(x)

    shortcut = Convolution2D(nb_filter3, 1, 1, subsample=strides,
                             name=conv_name_base + '1')(input_tensor)
    shortcut = BatchNormalization(axis=bn_axis, name=bn_name_base + '1')(shortcut)

    x = merge([x, shortcut], mode='sum')
    x = Activation('relu')(x)
    return x

def resnet50_model(img_rows, img_cols, color_type=1, num_classes=None):
    """
    Resnet 50 Model for Keras

    Model Schema is based on 
    https://github.com/fchollet/deep-learning-models/blob/master/resnet50.py

    ImageNet Pretrained Weights 
    https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels.h5

    Parameters:
      img_rows, img_cols - resolution of inputs
      channel - 1 for grayscale, 3 for color 
      num_classes - number of class labels for our classification task
    """

    # Handle Dimension Ordering for different backends
    global bn_axis
    if K.image_dim_ordering() == 'tf':
      bn_axis = 3
      img_input = Input(shape=(img_rows, img_cols, color_type))
    else:
      bn_axis = 1
      img_input = Input(shape=(color_type, img_rows, img_cols))

    x = ZeroPadding2D((3, 3))(img_input)
    x = Convolution2D(64, 7, 7, subsample=(2, 2), name='conv1')(x)
    x = BatchNormalization(axis=bn_axis, name='bn_conv1')(x)
    x = Activation('relu')(x)
    x = MaxPooling2D((3, 3), strides=(2, 2))(x)

    x = conv_block(x, 3, [64, 64, 256], stage=2, block='a', strides=(1, 1))
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='b')
    x = identity_block(x, 3, [64, 64, 256], stage=2, block='c')

    x = conv_block(x, 3, [128, 128, 512], stage=3, block='a')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='b')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='c')
    x = identity_block(x, 3, [128, 128, 512], stage=3, block='d')

    x = conv_block(x, 3, [256, 256, 1024], stage=4, block='a')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='b')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='c')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='d')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='e')
    x = identity_block(x, 3, [256, 256, 1024], stage=4, block='f')

    x = conv_block(x, 3, [512, 512, 2048], stage=5, block='a')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='b')
    x = identity_block(x, 3, [512, 512, 2048], stage=5, block='c')

    # Fully Connected Softmax Layer
    x_fc = AveragePooling2D((7, 7), name='avg_pool')(x)
    x_fc = Flatten()(x_fc)
    x_fc = Dense(1000, activation='softmax', name='fc1000')(x_fc)

    # Create model
    model = Model(img_input, x_fc)

    # Load ImageNet pre-trained data 
    if K.image_dim_ordering() == 'th':
      # Use pre-trained weights for Theano backend
      weights_path = 'imagenet_models/resnet50_weights_th_dim_ordering_th_kernels.h5'
    else:
      # Use pre-trained weights for Tensorflow backend
      weights_path = '/home/yash/Facial_Recognition/ResNet/pretrained/resnet50_weights_tf_dim_ordering_tf_kernels.h5'

    model.load_weights(weights_path)

    # Truncate and replace softmax layer for transfer learning
    # Cannot use model.layers.pop() since model is not of Sequential() type
    # The method below works since pre-trained weights are stored in layers but not in the model
    x_newfc = AveragePooling2D((7, 7), name='avg_pool')(x)
    x_newfc = Flatten()(x_newfc)
    x_newfc = Dropout(0.5)(x_newfc)
    x_newfc = Dense(num_classes, activation='softmax', name='fc10')(x_newfc)

    # Create another model with our customized softmax
    model = Model(img_input, x_newfc)

    # Learning rate is changed to 0.001
    sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True)
    model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
  
    return model

if __name__ == '__main__':

    # Example to fine-tune on 3000 samples from Cifar10

    img_rows, img_cols = 224, 224 # Resolution of inputs
    channel = 3
    num_classes = 8631
    batch_size = 32
    nb_epoch = 1000
    nb_steps = 1000
    train_data_dir = '/home/yash/Datasets/VGG-Face2/trains'
    validation_data_dir = '/home/yash/Datasets/VGG-Face2/validation'
    test_data_dir = '/home/yash/Datasets/VGG-Face2/test'

    # Load our model
    model = resnet50_model(img_rows, img_cols, channel, num_classes)

    #Image Input - Direct/Generator
    print("Images Loading")

    #Direct Input
    '''
    #build_hdf5_image_dataset(train_data_dir, image_shape=(224, 224), mode='folder', output_path='dataset_train.h5', categorical_labels=True, normalize=True, files_extension=['.jpg', '.png'])
    h5f = h5py.File('dataset_train.h5', 'r')
    X = h5f['X']
    X = np.array(X)
    Y = h5f['Y']
    Y = np.array(Y)

    print("Training Images loaded. Test Images Loading..")

    #build_hdf5_image_dataset(test_data_dir, image_shape=(224, 224), mode='folder', output_path='dataset_test.h5', categorical_labels=True, normalize=True, files_extension=['.jpg', '.png'])
    h5f = h5py.File('dataset_test.h5', 'r')
    Xtest = h5f['X']
    Xtest = np.array(Xtest)
    Ytest = h5f['Y']
    Ytest = np.array(Ytest)
    '''

    #Generator Input
    all_datagen = ImageDataGenerator(rescale=1./255)
    
    train_generator = all_datagen.flow_from_directory(train_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')
    validation_generator = all_datagen.flow_from_directory(validation_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')
    test_generator = all_datagen.flow_from_directory(test_data_dir, target_size=(224, 224), batch_size=batch_size, class_mode='categorical')

    print("Images Loaded.")

    # Start Fine-tuning
    print("Training Started")
    #plot_model(model, to_file='/home/yash/Facial_Recognition/ResNet/checkpoints/vggface2/model.png', show_shapes=True)
    tbCallBack = keras.callbacks.TensorBoard(log_dir='/home/yash/Facial_Recognition/ResNet/logs/vggface2', histogram_freq=0, write_graph=True, write_images=True)
    checkCallBack = keras.callbacks.ModelCheckpoint(filepath='/home/yash/Facial_Recognition/ResNet/checkpoints/vggface2/weights.hdf5', verbose=1, save_best_only=True, period=1)
    history = model.fit_generator(train_generator, steps_per_epoch=nb_steps, epochs= nb_epoch, verbose=2, callbacks=[tbCallBack,checkCallBack], validation_data=validation_generator, validation_steps=nb_steps)
    #hist = model.fit(x=X, y=Y, batch_size=batch_size, epochs=nb_epoch, verbose=2, callbacks=[tbCallBack], validation_split=0.1, shuffle=True, steps_per_epoch=None, validation_steps=None)
    print(history.history.keys())
    model.save('/home/yash/Facial_Recognition/ResNet/checkpoints/vggface2/vggface2_resnet50.h5')
    # summarize history for accuracy
    plt.plot(history.history['acc'])
    plt.plot(history.history['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('/home/yash/Facial_Recognition/ResNet/checkpoints/vggface2/accuracy.png')
    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'validation'], loc='upper left')
    plt.savefig('/home/yash/Facial_Recognition/ResNet/checkpoints/vggface2/loss.png')
    print("Training Done. Evaluation Started..")
    scores = model.evaluate_generator(validation_generator, steps=nb_steps*10, max_queue_size=10, workers=0, use_multiprocessing=False)
    #scores = model.evaluate(x=Xtest, y=Ytest, batch_size=32, verbose=2, sample_weight=None, steps=None)
    print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    print("All Done.")
